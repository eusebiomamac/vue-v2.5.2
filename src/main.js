// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App'
import router from './router'
import {googlemapsConfig} from './config/googlemapsConfig'
import Nav from '@/components/Nav'
import FlagIcon from 'vue-flag-icon'

//global.jQuery = require('jquery');
//var $ = global.jQuery;
//window.$ = global.jQuery;
//window.Vue = Vue;

const VueGoogleMaps = require('vue2-google-maps')
var VueFire = require('vuefire')

Vue.use(BootstrapVue)
Vue.use(FlagIcon);

Vue.use(VueGoogleMaps, {
  load: {
    key: googlemapsConfig.key,
    libraries: 'places'
  }
})

Vue.use(VueFire)

Vue.config.productionTip = false

Vue.component('navbar',Nav);


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
