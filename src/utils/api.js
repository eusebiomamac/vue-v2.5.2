import {firebase, db, storage} from 'firebase'
const api_endpoint_url = 'https://us-central1-mixx-8b601.cloudfunctions.net';

exports.api = {
	token: null,
	uid: null,

	prep(){
		var api = this;
    var user = firebase.auth().currentUser;
    var name, email, photoUrl, uid, emailVerified;

    if (user != null) {
      name = user.displayName;
      email = user.email;
      photoUrl = user.photoURL;
      emailVerified = user.emailVerified;
      uid = user.uid;  
      console.log('API: ' + uid);

      user.getIdToken(true)
        .then((t) =>{
          api.btoken = t.trim();
          console.log('API: back with token: ' + t);

        })
    }
	},

	getRecommendedVenues(_lat,_lng){
	  var api = this;
	  console.log('API: getRecommendedVenues() with: ' + api.btoken);
	  return axios.get(api_endpoint_url + '/api/places/lat/'+encodeURI(_lat) + '/lng/'+encodeURI(_lng) + '/recommend',
	  {
	    headers: { 
	      "Authorization": "Bearer " + api.btoken,
	      //'Access-Control-Allow-Origin': '*', 
	      'Content-Type': 'application/json'
	    },
	  })
	  .then((response) =>{
	    console.log(response);
	    return response.data;
	  })
	  .catch((error) =>{
	    console.error(error);
	  })
	},

	getUserEvents(_lat, _lng){

	  var api = this;
	  console.log('API: getUserEvents() with: ' + api.btoken);
	  return axios.get(api_endpoint_url + '/events/nearby/lat/'+encodeURI(_lat) + '/lng/'+encodeURI(_lng),
	  {
	    headers: { 
	      "Authorization": "Bearer " + api.btoken,
	      //'Access-Control-Allow-Origin': '*', 
	      'Content-Type': 'application/json'
	    },
	  })
	  .then((response) =>{
			console.log(response);
	  	return response.data;
	  })
	  .catch((error) =>{
	    console.error(error);
	  })
	}
}