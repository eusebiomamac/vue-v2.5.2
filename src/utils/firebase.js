import firebase from 'firebase'
import {firebaseConfig} from '../config/firebaseConfig'

firebase.initializeApp(firebaseConfig)

var db = firebase.database()
var storage = firebase.storage().ref()

export {firebase, db, storage}
