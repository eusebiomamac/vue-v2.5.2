import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Signup from '@/components/Signup'
import Home from '@/components/Home'
import AddEvent from '@/components/AddEvent'
import Inbox from '@/components/Inbox'
import Activities from '@/components/Activities'
import Messages from '@/components/Messages'
import ProfileTab from '@/components/ProfileTab'
import Events from '@/components/Events'
import Detail from '@/components/Detail'
import Splash from '@/components/Splash'
import User from '@/components/User'
import Auth from '@/components/Auth.vue'
import AuthSuccess from '@/components/AuthSuccess.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/auth',
      component: Auth
    },
    {
      path: '/success',
      component: AuthSuccess
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/inbox',
      name: 'Inbox',
      component: Inbox
    },
    {
      path: '/messages/:userId',
      name: 'Messages',
      component: Messages
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/add',
      name: 'Add Event',
      component: AddEvent
    },
    {
      path: '/activities',
      name: 'Activities',
      component: Activities
    },
    {
      path: '/events',
      name: 'Events',
      component: Events
    },
    {
      path: '/splash',
      name: 'Splash',
      component: Splash
    },
    {
      path: '/user/:userId',
      name: 'User',
      component: User
    },
    {
      path: '/profile',
      name: 'Profile',
      component: ProfileTab
    },
    {
      path: '/detail/:eventId',
      name: 'Detail',
      component: Detail
    }
  ]
})
